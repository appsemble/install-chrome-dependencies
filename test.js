#!/usr/bin/env node
import assert from 'assert';

import puppeteer from 'puppeteer';

/**
 * Test puppeteer dependencies are installed correctly by loading https://example.com.
 */
async function main() {
  const browser = await puppeteer.launch({ args: ['--no-sandbox'] });
  const page = await browser.newPage();
  await page.goto('https://example.com');
  const header = await page.$eval('h1', (el) => el.textContent);
  await browser.close();
  assert.equal(header, 'Example Domain');
}

main().catch((error) => {
  console.error(error);
  process.exit(1);
});
