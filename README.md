# Install Chrome Dependencies

> Install Chrome dependencies using a one liner

This is a simple script that installs dependencies for running Chrome / Chromium. This is useful
when running Chrome in a CI environment, for Example when using
[Puppeteer](https://github.com/puppeteer/puppeteer) or
[Lighthouse](https://github.com/GoogleChrome/lighthouse).

## Installation

```sh
npm install --save-dev install-chrome-dependencies
```

## Usage

```sh
npx install-chrome-dependencies
```

It’s recommended to use Debian based Docker images, such as `node:16-slim`.

For Debian based images, all required dependencies are installed to run Chrome inside the Docker
image.

For Alpine based images, `chromium` is installed. To use this with puppeteer, use the following
environment variable.

```sh
PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser
```
